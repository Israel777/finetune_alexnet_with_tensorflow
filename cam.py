import cv2
from validate import predictor

def show_webcam():
    cam = cv2.VideoCapture(0)
    while True:
        ret_val, img = cam.read()
        img = cv2.flip(img, 1)
        predictor(img)
        cv2.imshow('my webcam', img)
        if cv2.waitKey(1) == 'q':
            break
    cv2.destroyAllWindows()


# show_webcam()
imgs = []
imgs = cv2.imread('cat.jpg')
imgs = cv2.resize(imgs,(277,277))
print imgs.shape
predictor(imgs)